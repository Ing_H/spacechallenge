import models.Item
import models.Simulation
import models.U1
import models.U2

fun main(args: Array<String>){
    val sim:Simulation = Simulation()

    val itemsPhase1:List<Item> = sim.loadItems("src/main/resources/data/phase1.txt")
    val itemsPhase2:List<Item> = sim.loadItems("src/main/resources/data/phase2.txt")

    val u1Rockets:List<U1> = sim.loadU1(itemsPhase1)
    val u2Rockets:List<U2> = sim.loadU2(itemsPhase2)

    println("--------- Simulation for U1 --------------------")
    println("Budget required: " + sim.runSimulation(u1Rockets))

    println("--------- Simulation for U2 --------------------")
    println("Budget required: " + sim.runSimulation(u2Rockets))
}