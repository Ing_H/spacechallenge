package services

import models.Item

interface IFile {
    fun readFile(filename:String):MutableList<Item>
}