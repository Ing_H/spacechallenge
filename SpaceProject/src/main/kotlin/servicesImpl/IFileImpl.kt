package servicesImpl

import models.Item
import services.IFile
import java.io.File
import java.io.InputStream

class IFileImpl(): IFile {
    override fun readFile(filename: String):MutableList<Item> {
        var items: MutableList<Item> = mutableListOf()

        val inputStream: InputStream = File(filename).inputStream()
        inputStream.bufferedReader().forEachLine { items.add(makeItem(it)) }
        return items
    }

    private fun makeItem(text:String):Item{
        val array:List<String> = text.split("=")
        return Item(array[0],array[1].toInt())
    }
}