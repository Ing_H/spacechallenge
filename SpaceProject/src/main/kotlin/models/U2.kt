package models

class U2:Rocket(18000.0, 29000.0, 120_000_000){

    override fun launch() = willOperationSucceed(EXPLOSION_DELTA)

    override fun land() = willOperationSucceed(LANDING_DELTA)

    companion object {
        private const val EXPLOSION_DELTA = 0.04
        private const val LANDING_DELTA = 0.08
    }
}