package models

class U1:Rocket(10000.0, 18000.0, 100_000_000){

    override fun launch() = willOperationSucceed(EXPLOSION_DELTA)

    override fun land() = willOperationSucceed(LANDING_DELTA)

    companion object {
        private const val EXPLOSION_DELTA = 0.05
        private const val LANDING_DELTA = 0.01
    }
}