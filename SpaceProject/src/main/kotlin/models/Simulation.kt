package models

import extensions.activeEnergyShield
import services.IFile
import services.SpaceShip
import servicesImpl.IFileImpl

class Simulation {

    private val fileService:IFile = IFileImpl()

    fun loadItems(filename:String):MutableList<Item>{
        return fileService.readFile(filename)
    }

    fun loadU1(items:List<Item>):MutableList<U1>{
        var u1:U1
        var index:Int = 0
        var rocketsU1:MutableList<U1> = mutableListOf()

        while(index < items.size-1){
            u1 = U1()
            for(item in items.subList(index,items.size)){
                if(u1.canCarry(item)){
                    u1.carry(item)
                    index++
                } else {
                    break
                }
            }
            rocketsU1.add(u1)
        }
        return rocketsU1
    }

    fun loadU2(items:List<Item>):MutableList<U2>{
        var u2:U2
        var index:Int = 0
        var rocketsU2:MutableList<U2> = mutableListOf()

        while(index < items.size-1){
            u2 = U2()
            for(item in items.subList(index,items.size)){
                if(u2.canCarry(item)){
                    u2.carry(item)
                    index++
                } else {
                    break
                }
            }
            rocketsU2.add(u2)
        }
        return rocketsU2
    }

    fun runSimulation(rockets:List<Rocket>):Long {
        var index:Int = 0
        var totalBudget:Long = 0

        while(index < rockets.size) {
            var rocket:Rocket = rockets[index]

            if(rocket.launch() && rocket.land()) {
                totalBudget += rocket.cost
                index++
            } else {
                totalBudget += rocket.cost
            }
        }
        return totalBudget
    }
}