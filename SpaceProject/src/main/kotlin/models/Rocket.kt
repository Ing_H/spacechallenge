package models

import services.SpaceShip
import java.util.*

open class Rocket(
    var currentWeight:Double,
    val maxWeight:Double    ,
    val cost:Long
): SpaceShip {

    override fun launch() = true

    override fun land() = true

    override fun canCarry(item: Item) = calculateWeight(item) <= maxWeight

    override fun carry(item: Item) {
        currentWeight = calculateWeight(item)
    }

    private fun calculateWeight(item:Item) = item.weight + currentWeight

    protected fun generateRandomNum() = (1..9).random()

    protected fun willOperationSucceed(delta: Double) =
        (delta * (currentWeight / maxWeight)) *
                generateRandomNum() < SUCCESS_THRESHOLD

    companion object {
        private const val SUCCESS_THRESHOLD = 0.50
    }

}